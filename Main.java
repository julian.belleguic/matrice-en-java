import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
    static InputStreamReader isr = new InputStreamReader(System.in);
    static BufferedReader br = new BufferedReader(isr);
    public static final String RED = "\u001B[31m";
    public static final String GREEN = "\u001B[32m";
    public static final String RESET = "\u001B[0m";


    // Main
    public static void main(String[] args) throws IOException {
        /**
         * Programme principal
         * Contient le menu et intéragit avec le fichier Matrice.java
         * @throws IOException If an input or output exception occurred
         */
        // Condition pour le menu
        boolean run = true;
        // Boucle sur menu
        while(run) {
            System.out.println("Menu : ");
            System.out.println("1. Create matrice");
            System.out.println("2. Change value");
            System.out.println("3. Check dimension");
            System.out.println("4. Sum matrice");
            System.out.println("5. Is element");
            int choice = Integer.parseInt(br.readLine());
            // Switch
            switch (choice){
                // Creer une matrice
                case 1:
                    int lines = 0;
                    int columns = 0;
                    try {
                        // Réalise le bloc si aucune exception relevé
                        System.out.println("Nombre de line de la matrice : ");
                        lines = Integer.parseInt(br.readLine());
                        System.out.print("Nombre de column de la matrice : ");
                        columns = Integer.parseInt(br.readLine());
                        // Typage de la matrice + fonction creation matrice
                        ArrayList<ArrayList<Integer>> matrice = Matrice.create_matrice(lines, columns);
                        Matrice.print_matrice(matrice, "Voici votre matrice : ");
                        // Bloc pour relancer ou non le programme
                        System.out.println("");
                        System.out.print("Tap 'y' for continue else enter");
                        String continuer = br.readLine();
                        if(continuer.equals("Y") || continuer.equals("y")){
                            run = true;
                            break;
                        }
                        else {
                            run = false;
                            break;
                        }
                    }
                    catch (Exception e) {
                        // Réalise le bloc si une exception est relevé puis l'affiche
                        System.out.print(Main.RED + e + Main.RESET);
                        System.out.println("\n");
                        // Retour au début du menu
                        break;
                    }
                // Changer une value
                case 2:
                    int value;
                    try{
                        // Réalise le bloc si aucune exception relevé
                        // Typage de la matrice + fonction creation matrice
                        ArrayList<ArrayList<Integer>> matrice = Matrice.create_matrice(3, 4);
                        // Collecte de l'ensemble des paramètres nécessaires à la fonction change_value
                        Matrice.print_matrice(matrice);
                        System.out.println("line a modifier : ");
                        lines = Integer.parseInt(br.readLine());
                        System.out.println("column a modifier : ");
                        columns = Integer.parseInt(br.readLine());
                        System.out.println("value : ");
                        value = Integer.parseInt(br.readLine());
                        // Fonction qui permet le changement d'une value
                        Matrice.change_value(lines, columns, value, matrice);
                        // Bloc pour relancer ou non le programme
                        System.out.println("");
                        System.out.print("Tap 'y' for continue else enter");
                        String continuer = br.readLine();
                        if(continuer.equals("Y") || continuer.equals("y")){
                            run = true;
                            break;
                        }
                        else {
                            run = false;
                            break;
                        }
                    }
                    catch (Exception e) {
                        // Réalise le bloc si une exception est relevé puis l'affiche
                        System.out.print(Main.RED + e + Main.RESET);
                        System.out.println("\n");
                        // Retour au début du menu
                        break;
                    }
                case 3:
                    try{
                        System.out.println("Création des matrices : ");
                        System.out.println("Nb lignes matrice 1 : ");
                        lines = Integer.parseInt(br.readLine());
                        System.out.println("Nb colonnes matrice 1 : ");
                        columns = Integer.parseInt(br.readLine());
                        ArrayList<ArrayList<Integer>> matrice1 = Matrice.create_matrice(lines, columns);
                        System.out.println("Nb lignes matrice 2 : ");
                        lines = Integer.parseInt(br.readLine());
                        System.out.println("Nb colonnes matrice 2 : ");
                        columns = Integer.parseInt(br.readLine());
                        ArrayList<ArrayList<Integer>> matrice2 = Matrice.create_matrice(lines, columns);
                        if(Matrice.dimension_check(matrice1, matrice2)){
                            System.out.println("Les dimensions sont égales");
                        }
                        else{
                            System.out.println("Les dimensions ne sont pas égales");
                        }
                        // Bloc pour relancer ou non le programme
                        System.out.println("");
                        System.out.print("Tap 'y' for continue else enter");
                        String continuer = br.readLine();
                        if(continuer.equals("Y") || continuer.equals("y")){
                            run = true;
                            break;
                        }
                        else {
                            run = false;
                            break;
                        }
                    }
                    catch (Exception e){
                        // Réalise le bloc si une exception est relevé puis l'affiche
                        System.out.print(Main.RED + e + Main.RESET);
                        System.out.println("\n");
                        // Retour au début du menu
                        break;
                    }
                case 4:
                    try {
                        System.out.println("Création des matrices : ");
                        System.out.println("Nb lignes matrice 1 : ");
                        lines = Integer.parseInt(br.readLine());
                        System.out.println("Nb colonnes matrice 1 : ");
                        columns = Integer.parseInt(br.readLine());
                        ArrayList<ArrayList<Integer>> matrice1 = Matrice.create_matrice(lines, columns);
                        System.out.println("Nb lignes matrice 2 : ");
                        lines = Integer.parseInt(br.readLine());
                        System.out.println("Nb colonnes matrice 2 : ");
                        columns = Integer.parseInt(br.readLine());
                        ArrayList<ArrayList<Integer>> matrice2 = Matrice.create_matrice(lines, columns);
                        Matrice.sum_matrice(matrice1, matrice2);
                        // Bloc pour relancer ou non le programme
                        System.out.println("");
                        System.out.print("Tap 'y' for continue else enter");
                        String continuer = br.readLine();
                        if(continuer.equals("Y") || continuer.equals("y")){
                            run = true;
                            break;
                        }
                        else {
                            run = false;
                            break;
                        }
                    }
                    catch (Exception e){
                        // Réalise le bloc si une exception est relevé puis l'affiche
                        System.out.print(Main.RED + e + Main.RESET);
                        System.out.println("\n");
                        break;
                        // Retour au début du menu
                    }
                case 5:
                    try {
                        System.out.println("Crée votre matrice : ");
                        System.out.println("Nb lignes matrice : ");
                        lines = Integer.parseInt(br.readLine());
                        System.out.println("Nb colonnes matrice : ");
                        columns = Integer.parseInt(br.readLine());
                        ArrayList<ArrayList<Integer>> matrice = Matrice.create_matrice(lines, columns);
                        Matrice.print_matrice(matrice);
                        System.out.println("Quel nombre souhaitez vous vérifier : ");
                        int x = Integer.parseInt(br.readLine());
                        System.out.println("Recherche en cours ...");
                        double position = Matrice.is_element(matrice, x);
                        System.out.println("Position x,y de l'élément rechercher : " + position);
                        // Bloc pour relancer ou non le programme
                        System.out.println("");
                        System.out.print("Tap 'y' for continue else enter");
                        String continuer = br.readLine();
                        if(continuer.equals("Y") || continuer.equals("y")){
                            run = true;
                            break;
                        }
                        else {
                            run = false;
                            break;
                        }
                    }
                    catch (Exception e){
                        // Réalise le bloc si une exception est relevé puis l'affiche
                        System.out.print(Main.RED + e + Main.RESET);
                        System.out.println("\n");
                        // Retour au début du menu
                        break;

                    }
                default:
                    System.out.println(Main.RED + "Erreur de saisie" + Main.RESET);
                    break;
            }
        }
    }
}
