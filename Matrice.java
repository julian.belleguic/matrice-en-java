import java.util.ArrayList;
import java.io.IOException;

public class Matrice {
    public static void print_matrice(ArrayList<ArrayList<Integer>> matrice){
        /**
         * Fonction affichage d'une matrice
         * @param matrice ArrayList<ArrayList<Integer>> Matrice
         */
        System.out.print(matrice);
        System.out.println("\n");
    }

    // Fonction affichage matrice avec commentaire
    public static void print_matrice(ArrayList<ArrayList<Integer>> matrice, String message){
        /**
         * Fonction affichage d'une matrice avec message
         * @param matrice ArrayList<ArrayList<Integer>> Matrice
         * @param message String Message qui est transmit avec la matrice
         */
        System.out.print(message + matrice);
        System.out.println("\n");
    }

    // Fonction qui crée la matrice
    public static ArrayList<ArrayList<Integer>> create_matrice(int lines, int columns){
        /**
         * Fonction qui crée une matrice en fonction d'un nombre de lignes et de colonnes
         * @param lines int Represente le nombre de lignes
         * @param colonnes int Represente le nombre de colonnes
         * @throws IOException If an input or output exception occurred
         * @return ArrayList<ArrayList<Integer>> La matrice
         */
        // Initialisation matrice
        ArrayList<ArrayList<Integer>> matrice = new ArrayList<>();
        try{
            for(int i = 0; i < lines; i ++){
                // Boucle qui crée une line à chaque itération
                ArrayList<Integer> line = new ArrayList<>();
                for(int j = 0; j < columns; j ++){
                    // Boucle qui ajoute random à line sur chaque itération
                    int n = (int)(Math.random() * 9);
                    line.add(n);
                }
                // Boucle i qui ajoute line à la matrice sur chaque itération
                matrice.add(line);
            }
        }
        catch (Exception e){
            // Réalise le bloc si une exception est relevé puis l'affiche
            System.out.print(Main.RED + e + Main.RESET);
            System.out.println("\n");
            // Retour au début du menu
        }
        // Retourne la matrice
        return matrice;
    }

    // Fonction qui permet le changement de value
    public static ArrayList<ArrayList<Integer>> change_value(int line, int column, int value, ArrayList<ArrayList<Integer>> matrice){
        /**
         * Fonction qui change une valeur de la matrice
         * @param line int Represente la ligne à modifier
         * @param colonne int Represente la colonne à modifier
         * @throws IOException If an input or output exception occurred
         * @return ArrayList<ArrayList<Integer>> La matrice
         */
        try{
            matrice.get(line-1).set(column-1, value);
            Matrice.print_matrice(matrice, "Voici votre changement : ");
        }
        catch (Exception e)
        {
            // Réalise le bloc si une exception est relevé puis l'affiche
            System.out.print(Main.RED + e + Main.RESET);
            System.out.println("\n");
            // Retour au début du menu
        }
        return matrice;
    }

    public static boolean dimension_check(ArrayList<ArrayList<Integer>> matrice1, ArrayList<ArrayList<Integer>> matrice2){
        /**
         * Fonction qui compare deux matrice et détermine si les dimensions sont égales
         * @param matrice1 ArrayList<ArrayList<Integer>> Represente la première matrice
         * @param matrice2 ArrayList<ArrayList<Integer>> Represente la deuxième matrice
         * @return boolean
         */
        System.out.println("Vérification des dimensions");
        int matrice1_size = (matrice1.size() * matrice1.get(0).size());
        int matrice2_size = (matrice2.size() * matrice2.get(0).size());
        if(matrice1_size == matrice2_size){
            System.out.println(Main.GREEN + "OK" + Main.RESET);
            return true;
        }
        else {
            System.out.println(Main.RED + "Erreur" + Main.RESET);
            return false;
        }
    }
    public static ArrayList<ArrayList<Integer>> sum_matrice(ArrayList<ArrayList<Integer>> matrice1, ArrayList<ArrayList<Integer>> matrice2){
        ArrayList<ArrayList<Integer>> matrice_result = null;
        if(dimension_check(matrice1, matrice2)){
            print_matrice(matrice1, "Première matrice : ");
            print_matrice(matrice2, "Deuxième matrice : ");
            System.out.println("Calcul de la somme en cours");
            matrice_result = create_matrice(matrice1.size(), matrice1.get(0).size());
            for(int i = 0; i < matrice1.size(); i ++){
                for(int j = 0; j < matrice1.get(i).size(); j ++){
                    int result = matrice1.get(i).get(j) + matrice2.get(i).get(j);
                    matrice_result.get(i).set(j, result);
                }
            }
            print_matrice(matrice_result, "Voici la matrice somme : ");
            return matrice_result;
        }
        else {
            System.out.println(Main.RED + "Erreur de dimension" + Main.RESET);
            return matrice_result;
        }
    }
    public static double is_element(ArrayList<ArrayList<Integer>> matrice, int  x){
        double position = 0.0;
        if(matrice.contains(x)){
            for(int i = 0; i < matrice.size(); i ++){
                for(int j = 0; j < matrice.get(i).size(); j ++){
                    if(matrice.get(i).get(j) == x){
                        position = matrice.get(i).get(j);
                    }
                }
            }
        }
        return position;
    }
}
